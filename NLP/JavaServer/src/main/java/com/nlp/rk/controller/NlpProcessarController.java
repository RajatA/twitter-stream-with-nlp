package com.nlp.rk.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.nlp.rk.model.request.TextToAnalizeModel;
import com.nlp.rk.model.response.SentimentObjectModel;
import com.nlp.rk.services.SentimentAnalyzer;
import com.nlp.rk.services.SentimentResult;

/**
 * @class NlpProcessarController
 * @description handles REST API requests.
 * @author rajath
 *
 */
@RestController
public class NlpProcessarController {
	SentimentAnalyzer sentimentAnalyzer;

	public NlpProcessarController() {
		sentimentAnalyzer = new SentimentAnalyzer();
		sentimentAnalyzer.initialize();
	}

	// Handles request which hits "/nlp" API
	@CrossOrigin(origins = "http://localhost:3000")
	@PostMapping(path = "/nlp", consumes = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE,
					MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<?> getSentimentAnalisys(@RequestBody TextToAnalizeModel obj) {
		System.out.println("REQUEST IN : " + obj.getText());
		String cleans = obj.getText().replaceAll("[#@]", "");
		System.out.println("Cleansed String : " + cleans);
		String text = obj.getText();
		if (text == null || text == "") {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		SentimentResult sentimentResult = sentimentAnalyzer.getSentimentResult(text);
		SentimentObjectModel SOM = new SentimentObjectModel();

		SOM.setPositive(sentimentResult.getSentimentClass().getPositive());
		SOM.setVeryPositive(sentimentResult.getSentimentClass().getVeryPositive());
		SOM.setNeutral(sentimentResult.getSentimentClass().getNeutral());
		SOM.setNegative(sentimentResult.getSentimentClass().getNegative());
		SOM.setVeryNegative(sentimentResult.getSentimentClass().getVeryNegative());
		SOM.setSentimentScore(sentimentResult.getSentimentScore());
		SOM.setSentimentType(sentimentResult.getSentimentType());

		return new ResponseEntity<>(SOM, HttpStatus.OK);
	}

}
