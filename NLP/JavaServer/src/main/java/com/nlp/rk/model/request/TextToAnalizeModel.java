package com.nlp.rk.model.request;

/**
 * @class TextToAnalizeModel
 * @description helps to get request from the clients
 * @author rajath
 */
public class TextToAnalizeModel {
	private String text;

	public TextToAnalizeModel() {
		super();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
