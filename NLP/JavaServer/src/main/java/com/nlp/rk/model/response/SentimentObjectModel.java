package com.nlp.rk.model.response;

/**
 * @class SentimentObjectModel
 * @description Helps you to send the data to the user.
 * @author rajath
 */
public class SentimentObjectModel {
	private int positive;
	private int veryPositive;
	private int neutral;
	private int negative;
	private int veryNegative;
	private int sentimentScore;
	private String sentimentType;

	public int getPositive() {
		return positive;
	}

	public void setPositive(int positive) {
		this.positive = positive;
	}

	public int getVeryPositive() {
		return veryPositive;
	}

	public void setVeryPositive(int veryPositive) {
		this.veryPositive = veryPositive;
	}

	public int getNeutral() {
		return neutral;
	}

	public void setNeutral(int neutral) {
		this.neutral = neutral;
	}

	public int getNegative() {
		return negative;
	}

	public void setNegative(int negative) {
		this.negative = negative;
	}

	public int getVeryNegative() {
		return veryNegative;
	}

	public void setVeryNegative(int veryNegative) {
		this.veryNegative = veryNegative;
	}

	public int getSentimentScore() {
		return sentimentScore;
	}

	public void setSentimentScore(int sentimentScore) {
		this.sentimentScore = sentimentScore;
	}

	public String getSentimentType() {
		return sentimentType;
	}

	public void setSentimentType(String sentimentType) {
		this.sentimentType = sentimentType;
	}
}
