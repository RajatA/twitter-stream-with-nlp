# Twitter stream with NLP

servers 

* NodeJS server
* Java Spring Boot server

Front End

* ReactJS


### Description

This project uses its front end framework as React.js. Which is served by two micro services which runs on Node.JS and Java

**React.JS :**
A revolutionary front-end framework used for designing UI.  

**Node.JS :** is responsible for streaming the data from the Twitter It uses [_socket.io_](https://socket.io) for streaming the real-time data and [_Express.js_](https://expressjs.com) for serving REST requests.

**JAVA :** is responsible for applying NLP for requested texts.
It built using JAVA SpringBoot framework. Uses Tomcat server of serving requests. It uses [_Stanford Core NLP_](https://stanfordnlp.github.io/CoreNLP/download.html) Model for
its analysis.


## Installation

### Node Server 

Use the package manager [npm](https://www.npmjs.com/) to install Node packages.

Goto path : $>**Twitter stream with NLP/server/**
```bash
npm install 
```

To Run the Node server use below command.
```bash
npm start 
```
or
```bash
nodemon 
```

NodeJS server runs on **port 5000** 

### ReactJS Server 

Use the package manager [npm](https://www.npmjs.com/) to install Node packages.

Goto path : $>**Twitter stream with NLP/Client/**
```bash
npm install 
```

To Run the Node server use below command.
```bash
npm start 
```

React server runs on **port 3000** 

### JAVA server

#### Please download JAVA Jar file from the below this [Link](https://drive.google.com/file/d/1OLbQ2iXYywG_aWmh-LA2t56CqnH26b5E/view?usp=sharing) 

use below command to run the jar. *(Make sure you have installed and configured JDK)*.

Go to the downloaded folder 
```bash
PATH$> java -jar JavaServer-0.0.1-SNAPSHOT.jar
```
Then after the NLP server will start to run. 


---------------------


Run JAVA Server via Spring STS application

Select *NLP* as a root project instead of *JavaServer* (*JavaServer* present inside NLP folder) 

Project > Run As > Spring Boot App

above steps will run JavaProject, and Tomcat server starts to listen on port **4000**

## Image Links

[image1](images/z_1.png)

[image2](images/z_2.png)

[image3](images/z_3.png)
