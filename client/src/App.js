import React from 'react';
import Dashboard from './Components/Dashboard/Dashboard';
import StreamAnalisys from './Components/Dashboard/StreamAnalisys-Dashboard';
import TweetAnalizer from './Components/TweetAnalisys/TweetAnalizer';
import Nav from './Components/Navigation/Navigation';
import { BrowserRouter, Route} from 'react-router-dom';

import './App.css';
import './Tweet.css';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Nav/>
        <Route path="/" exact component = {Dashboard}/>
        <Route path="/analisys" exact component={StreamAnalisys} />
        <Route path="/analizer" exact component={TweetAnalizer}/>
      </div>
    </BrowserRouter>
  );
}

export default App;
