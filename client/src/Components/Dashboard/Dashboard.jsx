import React, { Component } from 'react';
import openSocket from 'socket.io-client';
import './Dashboard.css';
import Tweet from '../Tweets/Tweet';
import axios from 'axios';
import { connect } from 'react-redux';
import { addStreamedTweet } from '../../Store/Actions/Actions';
let socket;

/**
 * @class Dashboard
 * @description This component helps streams Real-time Tweets
 */
class Dashboard extends Component {

    state = ({
        tweets: []
    });

    componentWillUnmount() {
        console.log("componentWillUnmount Called");
        if (socket)
            socket.disconnect();
    }

    addTweet(twt) {

        this.props.addTweet(twt);
    }

    disconnect = () => {
        if (socket)
            socket.disconnect();
        console.log("Scoket : ", socket);
    }

    // Executes when the user clicks on Search button
    search = (event) => {
        if (!socket) {
            socket = openSocket("http://localhost:5000");
            socket.on('tweet', data => {
                console.log("Got Tweet!!!");
                if (data.action = 'create') {
                    this.addTweet(data);
                }
            });
        }
        else if (!socket.conneted) {
            socket.connect();
        }

        const term = document.getElementById("search").value;

        if (!term) {
            alert("Invalid Search term");
            return false;
        }

        const cleansedTerm = term.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');

        // Requests real-time tweets from node server, It provokes server socket 
        // to emit the real-time tweets
        axios.get('http://localhost:5000/tweet/stream?q=' + cleansedTerm).then(data => {
            console.log("Data Received : ", data);
        }).catch(err => {
            console.log("ERROR : ", err);
        });
    }

    render() {
        return (
            <div>
                <h1>Twitter Streaming Dashboard </h1>
                <div>
                    <input type="text" id="search" /> <button className="btn success" onClick={this.search}>Search</button>
                    <span className="stopBtn"><button className="btn danger" onClick={this.disconnect}>Stop Streaming</button></span>
                </div>
                <div>
                    {[... this.props.tweets].reverse().map(obj => <Tweet value={obj.tweet} key={obj.key} user={obj.user} />)}
                </div>
            </div>
        )
    };
}

// this method Maps State to Props
// Whatever Object you return from here you should refer this
// object using props.
const mapStateToProps = (state) => {
    console.log("Roots is : ", state)
    return {
        tweets : state.tweets
    };
}

// this method Maps dispatch method to Props
// Whatever Object you return from here you should refer this
// object using props.
const mapDispatchToProps = (dispatch) => {
    return {
        addTweet: (twt) => dispatch(addStreamedTweet(twt))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);