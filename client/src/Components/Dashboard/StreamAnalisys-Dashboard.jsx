import React, { Component } from 'react';
import axios from 'axios';
import Tweet from '../TweetAnalisys/Tweet';

/**
 * @class StreamAnanlysis
 * @description This component helps you to search Tweets and Analize it
 */
class StreamAnanlysis extends Component {

    state = {
        tweets:[]
    }

    searchTweets = () => {
        var self = this;
        // Requests tweets from node server
        axios.get("http://localhost:5000/tweet/search?q="+document.getElementById('name').value, null).then(function (resData){
            const data = resData.data.statuses;
            
            self.setState(prevState =>{
                let updatedTweets = {...prevState};
                updatedTweets.tweets = data;
                return updatedTweets;
            });
        }).catch(err =>{    
            console.log("Error", err);
        });
    }

    render() {
        return (
            <div>
                <h1>Tweet Search Dashboard</h1>
                
                <input type="text" name="searchbox" id="name" /> <button className="btn success" onClick={this.searchTweets.bind(this)}>Search</button>
            
                <div>
                    {this.state.tweets.map(obj => <Tweet value={obj.text} key={obj.id} user={obj.user}/>)}
                </div>
            </div>
        );
    }
}

export default StreamAnanlysis;