import React from 'react';
import { Link } from 'react-router-dom';

const nav = ()=> {
    return (
        <div className="Navs">
            <header>
                <nav>
                    <ul>
                        <li>
                            <Link to="/">Stream Twitter</Link>
                            <Link to={{
                              pathname: '/analisys'
                            }}>Analisys Dashboard</Link>
                        </li>
                    </ul>
                </nav>
            </header>
        </div>
    );
}

export default nav;