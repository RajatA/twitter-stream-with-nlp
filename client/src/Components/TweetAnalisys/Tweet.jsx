import React, { Component } from 'react';
import { Link } from 'react-router-dom';

/**
 * @class SearchTweets
 * @description This component populate as Analisys Dashboard 
 * search results.
 */
class SearchTweets extends Component {

    render() {
        let img;
        if (this.props.user) {
            img = <img align="left" src={this.props.user.profile_image_url} alt="profile_image" />;
        }

        return (<div className="Tweet">
            <div className="imgIcon cropImg"> {img} @{this.props.user.screen_name}
            <Link to={{pathname:"/analizer", state: {text: this.props.value, user:this.props.user}}}><button className="btn info rightAlign">Analize</button></Link>
             <br/><span className="userName">{this.props.user.name}</span>   
            <div className="userName"></div> </div>
            <div className="imgDesc">{this.props.value} </div>
        </div >
        );
    }

}

export default SearchTweets;