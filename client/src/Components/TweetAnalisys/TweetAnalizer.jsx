import React, { Component } from 'react';
import WordCloud from 'react-d3-cloud';
import axios from 'axios';

const fontSizeMapper = word => Math.log2(word.value) * 2;
const rotate = word => word.value % 360;

class TweetAnalizer extends Component {

    state = {
        analisys: {},
        result: false,
        data:[]
    }

    componentDidMount() {
        const split = this.props.location.state.text.split(" ");
        const max = 10000, min=1000;

        const mapText = () => split.map(t => {
            return { text: t, value: Math.floor(Math.random() * (+max - +min)) + +min };
        })
        axios.post('http://localhost:4000/nlp',{text:this.props.location.state.text}).then(result => {
        
            this.setState(prevState =>{
                const st = {...prevState};
                st.analisys = result.data;
                st.result = true;
                st.data = mapText();
                return st;
            });
        }).catch(err =>{ 
            console.log("ERROR", err);
        });        
    }

    render() {
        let analisysData

        console.log("USER : ", this.props.location.state.user);
        const user = this.props.location.state.user;
        let img;
        if(user) {
            img = <img align="left" src={user.profile_image_url} alt="profile_image" />;
        }
        if(this.state.result) {
            const {analisys} = this.state
            analisysData = <div className="analisys">
                Positive : {analisys.positive}% <br/>
                veryPositive : {analisys.veryPositive}% <br/>
                Negative : {analisys.negative}% <br/>
                veryNegative : { analisys.veryNegative}% <br/>
                Neutral : {analisys.neutral}% <br/>
                Sentiment Score : {analisys.sentimentScore}% <br/>
                Sentiment Type : {analisys.sentimentType} <br/>
            </div>
        }

        return (<div>
           <h2> Tweet Analisys Result </h2>
            <div className="Tweet">
                <div className="imgIcon cropImg"> {img} @{user.screen_name} <br/><span className="userName">{user.name}</span>   
                <div className="userName"></div> </div>
                <div className="imgDesc">{this.props.location.state.text} </div>
                <hr/>
                {analisysData}
            </div >
            
            <WordCloud
                data={this.state.data}
                fontSizeMapper={fontSizeMapper}
                // rotate={rotate}
                height={400}
                width={500}
            />
        </div>);
    }

}

export default TweetAnalizer;