import React, { Component } from 'react';

class Tweet extends Component {

    render() {
        let img;
        if (this.props.user) {
            img = <img className="cropImg" align="left" src={this.props.user.profile_image_url} alt="profile_image" />;
        }

        return (<div className="Tweet">
            <div className="imgIcon cropImg"> {img} @{this.props.user.screen_name} <br/><span className="userName">{this.props.user.name}</span>   
            <div className="userName"></div> </div>
            <div className="imgDesc">{this.props.value} </div>
        </div >
        );
    }

}

export default Tweet;