export const addStreamedTweet = (res) => {
    return (dispatch, getState) => {
        // you can do modification to the state here
        // setTimeOut is  just a callback function to make it look Async
        setTimeout(() => dispatch({ type: "ADD_STREAMING_TWEET", tweet: res }), 1000);
    }
}