require('dotenv').config();
require('./twitter').init();
const cors = require('cors');

const app = require('express')();

const PORT = process.env.PORT;

const server = app.listen(PORT, (args) => console.log('Express Server Listening on ', PORT));
const io = require('./socket').init(server);

app.use(cors());
app.use('/tweet', require('./routes/tweet.route'));

//Executes at the firest time the client connects to the system
io.on("connection", socket => {
  console.log("client connected!");
});

