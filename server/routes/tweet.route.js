const router = require('express').Router();
const client = require('../twitter').getClient();
const io = require('../socket').getIO();

// GET searching tweet
router.get('/search', (req, res) => {
    const { q } = req.query;
    if (!q) {
        return res.status(400).json({ message: "INVALID_PARAM" });
    }
    console.log("Searching : ", q);
    const client = require('../twitter').getClient();
    client.get('search/tweets.json', { q: q }, (error, tweets, response) => {
        if (error) {
            console.log("ERROR ::: ", error);
            return res.status(500).json(error);
        }
        return res.json(tweets);
    });
});


// GET Streaming real-time Tweets..
router.get('/stream', (req, res) => {
    try {
        console.log("Query : ", req.query);
        const { q } = req.query;
        if (!q) {
            return res.status(400).json({ message: "INVALID_PARAM" });
        }

        // Querying twitter for data
        client.stream('statuses/filter', { track: q }, function (stream) {
            stream.on('data', function (event) {
                console.log("data emited..");
                io.emit('tweet', { key: event.id, tweet: event.text, user: event.user });
            });

            stream.on('error', function (error) {
                // throw error;
                console.log("Error: ", error);
            });
        });
    } catch (err) {
        console.log("Something went wrong : ", err);
    }
});

module.exports = router;