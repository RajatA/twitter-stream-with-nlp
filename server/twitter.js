const Twitter = require('twitter');

let client

module.exports = {
    init: () => {
        client = new Twitter({
            consumer_key: process.env.CONSUMER_KEY,
            consumer_secret: process.env.CONSUMER_SECRET,
            access_token_key: process.env.ACCESS_TOKEN_KEY,
            access_token_secret: process.env.ACCESS_TOKEN_SECRET
        });
        return client;
    },
    getClient: () => {
        if (!client) 
            throw new Error('Twitter client is not initialized!');
        else
            return client;
    }
}